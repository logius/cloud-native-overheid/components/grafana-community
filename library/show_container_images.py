#!/usr/bin/python
#
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {'metadata_version': '0.1',
                    'status': ['preview'],
                    'supported_by': 'Team Standaard Platform'}

RETURN = r''' # '''

from ansible.module_utils.basic import AnsibleModule
import ansible.module_utils.basic
import yaml

def print_images(module):

    images = []
    for manifest_yaml in module.params['manifests'].split('\n---\n'): 
        manifest = yaml.safe_load(manifest_yaml)

        try:
            containers = manifest['spec']['template']['spec']['containers']
            for container in containers:
                images.append(container['image'])
        except KeyError:
            print('OK')

        try:
            containers = manifest['spec']['containers']
            for container in containers:
                images.append(container['image'])
        except KeyError:
            print('OK')

    return dict(images=images)

def main():
    """The main function."""
    module = AnsibleModule(
        argument_spec=dict(
            manifests=dict(type='str'),
        ),
        supports_check_mode=True)

    rst = print_images(module)

    module.exit_json(**rst)

if __name__ == '__main__':
    main()
