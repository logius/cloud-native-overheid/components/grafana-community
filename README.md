Grafana
=======

# Description
This Grafana role installs the grafana community helm chart from grafana.github.io.

The role is called `grafana-community` to make a distinction with the role `grafana`, which is based on the bitnami grafana chart.

https://github.com/grafana/helm-charts/tree/main/charts/grafana

This installation requires KeyCloak for OIDC and CertManager for LetsEncrypt certificates. 